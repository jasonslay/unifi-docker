FROM debian:jessie
# Based on the work of Jacob Alberty <jacob.alberty@foundigital.com>
MAINTAINER Jason Slay <jason.t.slay@gmail.com>

ARG DEB_FILE

VOLUME ["/var/lib/unifi", "/var/log/unifi", "/var/run/unifi", "/usr/lib/unifi/work"]

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -q update
RUN apt-get install -qy wget mongodb openjdk-7-jre binutils jsvc
RUN wget $DEB_FILE
RUN dpkg -i unifi_sysvinit_all.deb
RUN apt-get -q clean
RUN rm -rf /var/lib/apt/lists/*
RUN rm unifi_sysvinit_all.deb

RUN ln -s /var/lib/unifi /usr/lib/unifi/data
EXPOSE 8080/tcp 8081/tcp 8443/tcp 8843/tcp 8880/tcp 3478/udp

WORKDIR /var/lib/unifi

ENTRYPOINT ["/usr/bin/java", "-Xmx1024M", "-jar", "/usr/lib/unifi/lib/ace.jar"]
CMD ["start"]

