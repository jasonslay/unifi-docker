#! /bin/bash

docker build --no-cache -t jasonslay/unifi-docker:$1 --build-arg DEB_FILE=$2 . && docker push jasonslay/unifi-docker:$1
